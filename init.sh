#!/bin/sh


putgitrepo() { # Downlods a gitrepo $1 and places the files in $2 only overwriting conflicts
        dialog --infobox "Downloading and installing config files..." 4 60
        dir=$(mktemp -d)
        [ ! -d "$2" ] && mkdir -p "$2" && chown -R "$name:wheel" "$2"
        chown -R "$name:wheel" "$dir"
        sudo -u "$name" git clone --depth 1 "$1" "$dir/gitrepo" >/dev/null 2>&1 &&
        sudo -u "$name" cp -rfT "$dir/gitrepo" "$2"
        }

### vars
workspaceDir="$HOME/Workspace"
wpDir="$HOME/Pictures/Wallpapers"

### Setup folders
echo "** Creating required folders **\n"
mkdir -pv $workspaceDir
mkdir -pv $wpDir
mkdir -pv $HOME/.local/bin

echo "** Copying default wallpaper **"
# Either find default image, or DL new one
cp /usr/share/backgrounds/f$(rpm -E %fedora)/default/tv-wide/f$(rpm -E %fedora).png $wpDir

# Install everything needed
sudo dnf update -y
sudo dnf copr enable gregw/i3desktop
sudo dnf copr enable taw/Riot
sudo dnf config-manager --add-repo https://brave-browser-rpm-release.s3.brave.com/x86_64/
sudo rpm --import https://brave-browser-rpm-release.s3.brave.com/brave-core.asc
sudo dnf install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm
sudo dnf install -y curl git vim zsh i3-gaps brave-keyring brave-browser feh i3lock-color i3status w3m-img htop rofi ranger network-manager-applet light tldr neofetch the_silver_searcher

### Install non-dnf based things

# Betterlockscreen
curl https://raw.githubusercontent.com/pavanjadhaw/betterlockscreen/master/betterlockscreen -o $HOME/.local/bin/betterlockscreen
chmod u+x $HOME/.local/bin/betterlockscreen

# PyWal
pip3 install --user pywal 

# Joplin
wget -O - https://raw.githubusercontent.com/laurent22/joplin/master/Joplin_install_and_update.sh | bash


### Importing config

